module github.com/jenkins-zh/jenkins-cli

go 1.16

require (
	github.com/AlecAivazis/survey/v2 v2.2.13
	github.com/Netflix/go-expect v0.0.0-20180615182759-c93bf25de8e8
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/atotto/clipboard v0.1.4
	github.com/chai2010/gettext-go v0.0.0-20170215093142-bf70f2a70fb1
	github.com/creack/pty v1.1.11 // indirect
	github.com/golang/mock v1.5.0
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/go-github/v29 v29.0.3
	github.com/hashicorp/go-version v1.2.1
	github.com/hinshun/vt10x v0.0.0-20180616224451-1954e6464174
	github.com/jedib0t/go-pretty/v6 v6.2.3
	github.com/jenkins-zh/jenkins-formulas v0.0.5
	github.com/kr/pretty v0.2.1 // indirect
	github.com/linuxsuren/cobra-extension v0.0.10
	github.com/linuxsuren/go-cli-alias v0.0.6
	github.com/linuxsuren/go-cli-plugin v0.0.4
	github.com/linuxsuren/http-downloader v0.0.22
	github.com/magiconair/properties v1.8.5
	github.com/mitchellh/go-homedir v1.1.0
	github.com/onsi/ginkgo v1.15.1
	github.com/onsi/gomega v1.11.0
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.7.0
	github.com/zalando/go-keyring v0.0.0-20200121091418-667557018717
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	golang.org/x/text v0.3.5
	golang.org/x/tools v0.0.0-20210106214847-113979e3529a // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.4.0
	honnef.co/go/tools v0.0.1-2020.1.3 // indirect
	moul.io/http2curl v1.0.0
)
